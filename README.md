# User Details API

## Build locally
Enter the workspace and run `mvn clean install`

## Start the service
Run the command in the workspace 
`java -jar target/ing-user-details-1.0.jar` 

## Usage
#### Documentation
Swagger can be accessed at `http://localhost:8080/swagger-ui.html` 
and the open api docs at `http://localhost:8080/v3/api-docs/`

#### Security
The endpoints are authenticated with Basic Authentication. The following credentials can be used.

| Username | Password | Authorized |
| -------- | -------- | ---------- |
| user     | password | GET        |
| admin    | admin    | GET, PUT   |

#### Logging & Monitoring
All the requests log the type, headers, payload, response status and response body. 

## Examples
Sample requests are available as postman collection by importing `postman_collection.json` file.

## Production environment
Following are few areas of improvement for the api to be production ready

1. Improved exception handling and error output.
2. Improved logging format and monitoring to facilitate queries and dashboards.
3. DB storage instead of H2. Using H2 for local and test environemnts only.
4. Authentication servers for security and improved security over Basic Authentication.
5. Any Devops related updates ex: kubernetes.
6. Code coverage for tests ex:db timeouts test case

## To do

DB circuit breakers in the requirements of the challenge:  

I assume it refers to relaxed binding and auto reconnect, couldn't be sure. The code currently handles only db timeouts.
