package com.ing.user;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    private UserService userService;
    @Mock
    private UserRepository repository;

    @BeforeEach
    void setup() {
        userService = new UserService(repository);
    }

    @Test
    void get_shouldReturnEntity() {
        when(repository.findById(anyLong())).thenReturn(Optional.of(new User()));
        assertThat(userService.get(1L)).isNotEmpty();
    }

    @Test
    void get_shouldReturnEmptyIfEntityNotFound() {
        when(repository.findById(anyLong())).thenReturn(Optional.empty());
        assertThat(userService.get(1L)).isEmpty();
    }

}