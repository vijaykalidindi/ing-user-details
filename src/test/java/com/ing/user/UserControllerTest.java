package com.ing.user;

import static com.ing.user.TestUtils.prepareUser;
import static com.ing.user.TestUtils.prepareUserAsString;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

@WebMvcTest
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserService userService;

    private static final String API_USERDETAILS = "/api/userdetails/";

    @Test
    void getUserDetails_shouldThrow_401_forUnauthorized() throws Exception {
        mockMvc.perform(get(API_USERDETAILS + "5"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void getUserDetails_shouldNotThrow_401_forAuthorized_USERRole() throws Exception {
        mockMvc.perform(get(API_USERDETAILS + "5"))
                .andExpect(status().is(not(401)));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void getUserDetails_shouldNotThrow_401_forAuthorized_ADMINRole() throws Exception {
        mockMvc.perform(get(API_USERDETAILS + "5"))
                .andExpect(status().is(not(401)));
    }

    @Test
    void updateUserDetails_should_throw_401_forUnauthorized() throws Exception {
        mockMvc.perform(put(API_USERDETAILS + "5").content(""))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void updateUserDetails_shouldThrow_403_forAuthorized_USERRole() throws Exception {
        mockMvc.perform(put(API_USERDETAILS + "5"))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateUserDetails_shouldNotThrow_401or403_forAuthorized_ADMINRole() throws Exception {
        mockMvc.perform(put(API_USERDETAILS + "5"))
                .andDo(print())
                .andExpect(status().is(not(401)))
                .andExpect(status().is(not(403)));
    }

    @Test
    @WithMockUser
    void getUserDetails_shouldThrowBadRequest() throws Exception {
        mockMvc.perform(get(API_USERDETAILS + "5ee"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateUserDetails_shouldThrowBadRequest() throws Exception {
        mockMvc.perform(put(API_USERDETAILS + "5ee"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void getUserDetails_shouldThrowNotFound() throws Exception {
        when(userService.get(anyLong())).thenReturn(Optional.empty());
        mockMvc.perform(get(API_USERDETAILS + "55"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateUserDetails_shouldThrowNotFound() throws Exception {
        mockMvc.perform(put(API_USERDETAILS + "55")
                .content(prepareUserAsString())
                .contentType(APPLICATION_JSON_UTF8_VALUE))
                // Had to use this deprecated type as workaround for bug in mockMvc
                // that appends charset_ISO_8859_1 to content type and throws
                // "application/json;charset_ISO_8859_1 not supported" in spring boot 2.3.x
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateUserDetails_shouldUpdateWithOk() throws Exception {
        User user = prepareUser();
        String userUpdate = prepareUserAsString();
        when(userService.update(5L, user)).thenReturn(Optional.of(user));
        mockMvc.perform(put(API_USERDETAILS + "5")
                .content(userUpdate)
                .contentType(APPLICATION_JSON_UTF8_VALUE))
                // Had to use this deprecated type as workaround for bug in mockMvc
                // that appends charset_ISO_8859_1 to content type and throws
                // "application/json;charset_ISO_8859_1 not supported" in spring boot 2.3.x
                .andExpect(status().isOk())
                .andExpect(content().json(userUpdate));
    }
}