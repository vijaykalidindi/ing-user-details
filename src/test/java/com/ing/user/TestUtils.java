package com.ing.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.jetbrains.annotations.NotNull;

class TestUtils {
    @NotNull
    public static User prepareUser() {
        User user = new User();
        user.setTitle("Mr");
        user.setFirstname("test");
        user.setLastname("lastname");
        user.setGender("male");

        UserAddress address = new UserAddress();
        address.setStreet("12345 holling rd");
        address.setCity("Sydney");
        address.setState("nsw");
        address.setPostcode(2000);

        user.setAddress(address);
        return user;
    }

    public static String prepareUserAsString() {
        try {
            return new ObjectMapper().writeValueAsString(prepareUser());
        } catch (JsonProcessingException ignored) {
        }
        return null;
    }
}
