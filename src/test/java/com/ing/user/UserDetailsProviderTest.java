package com.ing.user;

import static com.ing.user.TestUtils.prepareUser;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import au.com.dius.pact.provider.junit.Provider;
import au.com.dius.pact.provider.junit.State;
import au.com.dius.pact.provider.junit.loader.PactFolder;
import au.com.dius.pact.provider.junit5.HttpTestTarget;
import au.com.dius.pact.provider.junit5.PactVerificationContext;
import au.com.dius.pact.provider.junit5.PactVerificationInvocationContextProvider;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        properties = "server.port=8080")
@Provider("userdetailsservice")
@PactFolder("pacts")
class UserDetailsProviderTest {
    @MockBean
    private UserService userService;

    @BeforeEach
    void setupTestTarget(PactVerificationContext context) {
        context.setTarget(new HttpTestTarget("localhost", 8080, "/"));
    }

    @TestTemplate
    @ExtendWith(PactVerificationInvocationContextProvider.class)
    void pactVerificationTestTemplate(PactVerificationContext context) {
        context.verifyInteraction();
    }

    @State({"provider receives a user details request"})
    public void toGetUserDetails() {
        User user = prepareUser();

        when(userService.get(1L)).thenReturn(Optional.of(user));
    }

    @State({"provider accepts a user details update"})
    public void toPutUserDetails() {
        User user = prepareUser();
        when(userService.get(1L)).thenReturn(Optional.of(user));

        User updated = prepareUser();
        updated.setLastname("updated");
        updated.getAddress().setCity("Newcastle");
        when(userService.update(eq(1L), any(User.class))).thenReturn(Optional.of(updated));
    }



}
