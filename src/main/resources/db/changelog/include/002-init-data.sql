--liquibase formatted sql

--changeset init_tables:002
insert into user values(null, 'Mr', 'test', 'lastname', 'male', 1);
insert into user values(null, 'Mr', 'test', 'lastname', 'male', 2);
insert into user values(null, 'Mr', 'test', 'lastname', 'male', 3);
insert into user values(null, 'Ms', 'test', 'lastname', 'male', 4);
insert into user values(null, 'Ms', 'test', 'lastname', 'male', 5);

insert into user_address values(null, '12345 holling rd', 'Sydney', 'nsw', 2000);
insert into user_address values(null, '12345 holling rd', 'Sydney', 'nsw', 2000);
insert into user_address values(null, '12345 holling rd', 'Sydney', 'nsw', 2000);
insert into user_address values(null, '12345 holling rd', 'Sydney', 'nsw', 2000);
insert into user_address values(null, '12345 holling rd', 'Sydney', 'nsw', 2000);
