--liquibase formatted sql

--changeset Create_tables:001

create table user_address (
    id bigint primary key auto_increment,
    street varchar(50) not null,
    city varchar(50) not null,
    state varchar(50) not null,
    postcode integer not null
);

create table user (
    id bigint primary key auto_increment,
    title varchar(5) not null,
    firstname varchar(50) not null,
    lastname varchar(50) not null,
    gender varchar(10),
    user_address_id bigint ,
    foreign key(user_address_id) references user
);
