package com.ing.common;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import java.io.IOException;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
public class LoggingFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        ContentCachingRequestWrapper requestToUse = new ContentCachingRequestWrapper(request);
        ContentCachingResponseWrapper responseToUse = new ContentCachingResponseWrapper(response);
        log.info("Request Received : " + requestMessage(requestToUse));
        try {
            filterChain.doFilter(requestToUse, responseToUse);
        } finally {
            log.info("Request Processed : " + responseMessage(requestToUse, responseToUse));
        }
    }

    private String requestMessage(HttpServletRequest request) {
        HttpHeaders headers = new ServletServerHttpRequest(request).getHeaders();
        return request.getMethod() + " " + request.getRequestURI() + " headers=" + headers;
    }

    private String responseMessage(ContentCachingRequestWrapper request, ContentCachingResponseWrapper response) throws IOException {
        StringBuilder msg = new StringBuilder();
        msg.append("status=").append(response.getStatus()).append(" ")
                .append("headers=")
                .append(response.getHeaderNames().stream()
                        .collect(Collectors.toMap(Function.identity(), response::getHeader, (n, h) -> n + ", " + h)))
                .append(" ")
                .append("payload=").append(new String(request.getContentAsByteArray())).append(" ")
                .append("response=").append(new String(response.getContentAsByteArray())).append(" ");
        response.copyBodyToResponse(); // IMPORTANT: copy content of response back into original response
        return msg.toString();
    }
}
