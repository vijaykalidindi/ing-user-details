package com.ing.common;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class ApiExceptionHandler {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = INTERNAL_SERVER_ERROR)
    @ResponseBody
    public Map<String, Object> handleGenericException(Exception e) {
        log.error(e.getMessage(), e);
        return getErrorMap(500, "Intenal Server Error", e.getMessage());
    }


    @ExceptionHandler(ResponseStatusException.class)
    @ResponseBody
    public ResponseEntity<Map<String, Object>> handleResponseStatusException(ResponseStatusException e) {
        log.error(e.getMessage(), e);
        return ResponseEntity.status(e.getStatus())
                .body(getErrorMap(e.getStatus().value(), e.getReason(), e.getMessage()));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    public Map<String, Object> handleMethodArgumentNotValidException(final MethodArgumentNotValidException e) {
        log.error(e.getMessage(), e);
        Map<String, Object> map = getErrorMap(400, "Bad Request", "Validation failed");
        Map<String, String> fieldErrors = e.getBindingResult().getAllErrors().stream()
                .map(o -> (FieldError) o)
                .collect(Collectors.toMap(FieldError::getField, fieldError -> fieldError.getDefaultMessage()
                        , (f, s) -> f + ", " + s));
        map.put("fieldErrors", fieldErrors);
        return map;
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    public Map<String, Object> handleMethodArgumentNotValidException(final MethodArgumentTypeMismatchException e) {
        log.error(e.getMessage(), e);
        Map<String, Object> map = getErrorMap(400, "Bad Request", "Type conversion failed");
        map.put("paramErrors", Collections.singletonMap(e.getName(), e.getValue()));
        return map;
    }

    private Map<String, Object> getErrorMap(int status, String error, String message) {
        Map<String, Object> map = new HashMap<>();
        map.put("status", status);
        map.put("error", error);
        map.put("message", message);
        map.put("timestamp", LocalDateTime.now());
        return map;
    }
}
