package com.ing;

import static org.springframework.http.HttpMethod.GET;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;

@EnableWebSecurity
class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //HTTP Basic authentication
        http.authorizeRequests()
                .antMatchers(GET,"/api/userdetails/**").hasAnyRole("ADMIN", "USER")
                .antMatchers("/api/userdetails/**").hasRole("ADMIN")
                .anyRequest().permitAll()
                .and().httpBasic()
                .and().csrf().disable();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser(User.builder().username("user").password("{noop}password").roles("USER"))
                .withUser(User.builder().username("admin").password("{noop}admin").roles("ADMIN"));
    }

}

