package com.ing.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
interface UserRepository extends JpaRepository<User, Long> {
}
