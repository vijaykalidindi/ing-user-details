package com.ing.user;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping(path = "/api/userdetails/{id}", produces = APPLICATION_JSON_VALUE)
@AllArgsConstructor
@Tag(name = "Word Count", description = "This API provides functionality for word count in given text.")
class UserController {

    private UserService userService;

    @Operation(responses = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(example =""))),
            @ApiResponse(responseCode = "401", description = "Please login", content = @Content(schema = @Schema(example =""))),
            @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content(schema = @Schema(example =""))),
            @ApiResponse(responseCode = "404", description = "Not Found", content = @Content(schema = @Schema(example =""))),
            @ApiResponse(responseCode = "500", description = "Server Error", content = @Content(schema = @Schema(example ="")))
    }, security = @SecurityRequirement(name = "basicAuth"))
    @GetMapping
    @ResponseStatus(value = OK)
    User getUserDetails(@PathVariable Long id) {
        return userService.get(id).orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "User not found"));
    }

    @Operation(responses = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(example =""))),
            @ApiResponse(responseCode = "401", description = "Please login", content = @Content(schema = @Schema(example =""))),
            @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content(schema = @Schema(example =""))),
            @ApiResponse(responseCode = "404", description = "Not Found", content = @Content(schema = @Schema(example =""))),
            @ApiResponse(responseCode = "500", description = "Server Error", content = @Content(schema = @Schema(example ="")))
    }, security = @SecurityRequirement(name = "basicAuth"))
    @PutMapping
    @ResponseStatus(value = OK)
    User updateUserDetails(@PathVariable Long id, @Valid @RequestBody User user) {
        return userService.update(id, user).orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "User not found"));
    }
}
