package com.ing.user;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
class UserService {
    private UserRepository repository;

    Optional<User> get(Long id) {
        return repository.findById(id);
    }

    @Transactional(timeout = 5)
    public Optional<User> update(Long id, User user) {
        return get(id)
                .map(existing -> {
                    user.setId(id);
                    user.getAddress().setId(existing.getAddress().getId());
                    return repository.save(user);
                });
    }
}
